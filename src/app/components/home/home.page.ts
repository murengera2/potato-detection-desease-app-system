import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxImageCompressService } from 'ngx-image-compress';
import { File } from '@ionic-native/file/ngx';
import { ProgressBarModule } from 'angular-progress-bar';
import imageCompression from 'browser-image-compression';
import rgbaToRgb from 'rgba-to-rgb';
import {
  IonicModule,
  LoadingController,
  Platform,
  ToastController,
} from '@ionic/angular';
import {
  Camera,
  CameraResultType,
  CameraSource,
  Photo,
} from '@capacitor/camera';
import { ApiService } from '../api.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Filesystem, Directory } from '@capacitor/filesystem';

interface LocalFile {
  name: string;
  path: string;
  data: string;
}
export interface UserPhoto {
  filepath: string;
  webviewPath?: string;
}
export class PhotoService {
  public photos: UserPhoto[] = [];
  private PHOTO_STORAGE: string = 'photos';

  // other code
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  standalone: true,
  imports: [IonicModule, HttpClientModule, CommonModule, RouterModule],
  providers: [ApiService, File],
})
export class HomePage implements OnInit {
  imagSource: any;
  images: any;
  data: any = [];
  public buffer = 0;
  public progress = 0;
  isLoading: boolean = false;
  imgResultBeforeCompression: string = '';
  imgResultAfterCompression: string = '';
  imgSrc?: string;
  dataUrl?: string;
  imgFile?: string;

  response: any = [];
  public photos: UserPhoto[] = [];
  constructor(
    private api: ApiService,
    private plt: Platform,
    private http: HttpClient,
    private imageCompress: NgxImageCompressService,

    private toastCtrl: ToastController,
    private platform: Platform,
    
    private loadingController: LoadingController
  ) {}

  async ngOnInit() {}
  async presentLoading() {
 
  }

  async uploadData(formData: FormData) {
    console.log('starting loading');
   await this.loadingController.create({
     spinner: 'circles',
     keyboardClose: true,
     message: 'Logging you in, Please Wait'
   });


     await this.presentLoading(); //
    const path = `predict`;
    this.api.postDatas(path, formData).subscribe(async (resp: any) => {
      this.data = resp;
      this.loadingController.dismiss();  
    });
     
  }
 
  takePicture = async () => {
    const image = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Prompt,
    quality:100,
      width: 256,
      height:256,
      allowEditing: false,
    });

    // console.log(image);

    const cameraResult = await this.savePicture(image);
    let blob = await fetch(cameraResult.webviewPath).then((r) => r.blob());
    this.images=image['webPath']

    console.log(blob['size'], blob['type'], blob);
    let formData = new FormData();
    formData.append('file', blob, blob['type']);
    let path = `predict`;
    this.api.postDatas(path, formData).subscribe((res) => {
      this.response=res
      console.log(this.response);
    });
    console.log('ff', formData);
  };

  async savePicture(photo: any) {
    // Convert photo to base64 format, required by Filesystem API to save
    const base64Data = await this.readAsBase64(photo);

    // Write the file to the data directory
    const fileName = Date.now() + '.jpeg';
    const savedFile = await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: Directory.Data,
    });

    // Use webPath to display the new image instead of base64 since it's
    // already loaded into memory
    return {
      filepath: fileName,
      webviewPath: photo.webPath,
    };
  }

  private async readAsBase64(photo: Photo) {
    // Fetch the photo, read as a blob, then convert to base64 format
    const response = await fetch(photo.webPath!);
    const blob = await response.blob();

    return (await this.convertBlobToBase64(blob)) as string;
  }

  private convertBlobToBase64 = (blob: Blob) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();

      reader.onerror = reject;
      reader.onload = () => {
        resolve(reader.result);
      };
      reader.readAsDataURL(blob);
    });


  // takePicture = async () => {
  //   const image = await Camera.getPhoto({
  //     resultType: CameraResultType.Uri,
  //     source: CameraSource.Prompt,

  //     allowEditing: false,
  //   });

  //   await this.savePicture(image);

  //   this.images = image['webPath'];

  //   //       let blob = await fetch(cameraResult.webviewPath).then((r) => r.blob());

  //   //       console.log("***************  blob size before compressed*****",blob.size)
  //   //       let formData = new FormData();
  //   //       formData.append('file', blob,blob['type']);
  //   //       let path = `predict`;
  //   //       // const loading = await this.loadingCtrl.create({
  //   //       //   message: 'Analyising image..........',
  //   //       //   translucent: true,
  //   //       // });

  //   //       // await loading.present();
  //   //       console.log("*************** compressed  blob file size *****",blob.size)
  //   //       this.api.postDatas(path, formData).subscribe(
  //   //         async (res) => {
  //   //           this.isLoading = false;
  //   //           this.response = res;

  //   //           // await loading.dismiss();
  //   //         },
  //   //         (error) => {
  //   //           this.isLoading = false;

  //   //           this.api.alert(
  //   //             'ERROR',
  //   //             '',
  //   //             'Unable to login, wrong username or password',
  //   //             'RETRY'
  //   //           );
  //   //         }
  //   //       );
  // };
  // onImageChange(e: any) {
  //   const reader = new FileReader();
  //   if (e.target.files && e.target.files.length) {
  //     const imgfile = e.target.files[0];
  //     reader.readAsDataURL(imgfile);
  //     reader.onload = async () => {
  //       this.imgSrc = reader.result as string;
  //       const img = document.createElement('img');
  //       const canvas = document.createElement('canvas');
  //       img.src = this.imgSrc;
  //       canvas.width = 100;
  //       canvas.height = 100;

  //       this.dataUrl = canvas.toDataURL(this.imgSrc);
  //       console.log('data url', this.dataUrl);
  //       this.imgFile = this.dataUrl; // bind with a preview-div
  //       let blob = await fetch(this.imgFile).then((r) => r.blob());
  //       let formData = new FormData();
  //       formData.append('file', blob, blob['type']);
  //       let path = `predict`;
  //       this.api.postDatas(path, formData).subscribe(
  //         async (res) => {
  //           this.isLoading = false;
  //           this.response = res;

  //           // await loading.dismiss();
  //         },
  //         (error) => {
  //           this.isLoading = false;

  //           this.api.alert(
  //             'ERROR',
  //             '',
  //             'Unable to login, wrong username or password',
  //             'RETRY'
  //           );
  //         }
  //       );

  //       console.log('image ile *********', blob);
  //     };
  //   }
  // }
  // async savePicture(photo: any) {

    
  //   // Convert photo to base64 format, required by Filesystem API to save
  //   const base64Data = await this.readAsBase64(photo);

  //   // Write the file to the data directory
  //   const fileName = Date.now() + '.jpeg';
  //   const savedFile = await Filesystem.writeFile({
  //     path: fileName,
  //     data: base64Data,
  //     directory: Directory.Data,
  //   });

  //   // Use webPath to display the new image instead of base64 since it's
  //   // already loaded into memory
  //   return {
  //     filepath: fileName,
  //     webviewPath: photo.webPath,
  //   };
  // }

  // private async readAsBase64(photo: Photo) {
  //   // Fetch the photo, read as a blob, then convert to base64 format
  //   const response = await fetch(photo.webPath!);
  //   const blob = await response.blob();

  //   return (await this.convertBlobToBase64(blob)) as string;
  // }

  // private convertBlobToBase64 = (blob: Blob) =>
  //   new Promise(() => {
  //     const reader = new FileReader();
  //     reader.readAsDataURL(blob);
  //     reader.onload = async () => {
  //       this.imgSrc = reader.result as string;

  //       const img = document.createElement('img');
  //       const canvas = document.createElement('canvas');
  //       img.src = this.imgSrc;
        
        
  //       const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
  //       ctx.drawImage(img, 0, 0, 256, 256); // image resize to 100x100
  //       this.dataUrl = canvas.toDataURL(img.src);
  //       this.imgFile = this.dataUrl;
  //       // bind with a preview-div
  //       let blob = await fetch(this.imgFile).then((r) => r.blob());
  //       let formData = new FormData();
  //       formData.append('file', blob, blob['type']);
  //       let path = `predict`;
  //       this.api.postDatas(path, formData).subscribe(
  //         async (res) => {
  //           this.isLoading = false;
  //           this.response = res;

  //           // await loading.dismiss();
  //         },
  //         (error) => {
  //           this.isLoading = false;

  //           this.api.alert(
  //             'ERROR',
  //             '',
  //             'Unable to login, wrong username or password',
  //             'RETRY'
  //           );
  //         }
  //       );

  //       console.log('image file *********', blob);
  //     };
  //   });
}
