import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  NgForm,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { ApiService } from '../api.service';
import { HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  standalone: true,
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [ApiService],
})
export class LoginPage implements OnInit {
  submitted = false;
  loading = false;
  user = {};
  loginForm: FormGroup;
  username: string = '';
  profile: any;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private api: ApiService,
    private fb: FormBuilder
  ) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  ngOnInit() {}
  disableForm() {
    this.loginForm?.get('email')?.disable();
    this.loginForm?.get('password')?.disable();
  }

  enableForm() {
    this.loginForm?.get('email')?.enable();
    this.loginForm?.get('password')?.enable();
  }

  onLogin(form: NgForm) {
    this.loading = true;
    this.api.login(form).subscribe(
      (response) => {
         this.profile = response;
         window.localStorage.setItem('profile', JSON.stringify(this.profile));
         
      
        console.log("response:",response);
        this.loading = false;

        this.router.navigate(['/home']);
      },
      (error) => {
        this.loading = false;

        this.api.alert(
          'ERROR',
          '',
         'Invalid Credentials'
        );
      }
    );
  }
}
