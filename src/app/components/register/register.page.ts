import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, FormsModule, NgForm, ReactiveFormsModule, Validators } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import {ActivatedRoute, Router, RouterModule} from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from '../api.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule,RouterModule,HttpClientModule,ReactiveFormsModule],
  providers: [ApiService],
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  loading = false;
  constructor( public router: Router,
    private route: ActivatedRoute,
    private api: ApiService,
    private fb: FormBuilder) {
    this.registerForm = this.fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
   }

  ngOnInit() {
  }

  disableForm() {
    this.registerForm?.get('email')?.disable();
    this.registerForm?.get('password')?.disable();
  }

  enableForm() {
    this.registerForm?.get('email')?.enable();
    this.registerForm?.get('password')?.enable();
  }

  register(form: NgForm) {
 
    
    this.loading = true;
    this.api.register(form).subscribe(
      (response: any) => {
        this.loading = false;
      this.router.navigate(['/login']);
      },
      (error) => {
        this.loading = false;

        this.api.alert(
          'ERROR',
          '',
          'Something Want wrong,Please try again!',
          'RETRY'
        );
      }
    );
  
}

}
