import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class ApiService {

  apiRoot = 'http://192.168.1.67:8000/'
    
  // apiRoot = 'http://44.211.255.218/'
  // apiRoot='http://ec2-54-145-241-62.compute-1.amazonaws.com/'
  //  apiRoot ='http://localhost:8000/';
    
  // apiRoot='https://ec2-54-145-241-62.compute-1.amazonaws.com/'
 
  
  httpOptions = {
    headers: new HttpHeaders({
      enctype: 'multipart/form-data',
    }),
  };

  constructor(
    private http: HttpClient,
    public alertController: AlertController
  ) {}

  postDatas(path: string, data: any) {
    const url = `${this.apiRoot}${path}`;
    return this.http.post(url, data);
  }

  postFormData(path: string, data: any) {
    const url = `${this.apiRoot}${path}`;
    console.log(url);
    return this.http.post(url, data, this.httpOptions);
  }
  login(params: any) {
    const url = this.apiRoot + 'login';
    return this.http.post(url, params, {});
  }
  register(params: any) {
    const url = this.apiRoot + 'signup';
    return this.http.post(url, params, {});
  }
  async alert(
    title: string = 'Alert',
    subTitle: string = '',
    text: string,
    action: string = 'Try'
  ) {
    const alert = await this.alertController.create({
      header: title,
      subHeader: subTitle,
      message: text,
      buttons: [action],
    });

    await alert.present();
  }
}
