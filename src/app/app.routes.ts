import { Routes } from '@angular/router';

export const routes: Routes = [
  // {
  //   path: 'welcome',
  //   loadComponent: () => import('./components/welcome/welcome.page').then( m => m.WelcomePage)
  // },
  {
    path: '',
    redirectTo: 'welcome',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadComponent: () => import('./components/home/home.page').then((m) => m.HomePage),
  },

  {
    path: 'home',
    loadComponent: () => import('./components/home/home.page').then((m) => m.HomePage),
  },
  {
    path: 'splash',
    loadComponent: () => import('./components/splash/splash.page').then( m => m.SplashPage)
  },
  {
    path: 'login',
    loadComponent: () => import('./components/login/login.page').then( m => m.LoginPage)
  },
  {
    path: 'register',
    loadComponent: () => import('./components/register/register.page').then( m => m.RegisterPage)
  },
  {
    path: 'welcome',
    loadComponent: () => import('./components/welcome/welcome.page').then( m => m.WelcomePage)
  },
];
