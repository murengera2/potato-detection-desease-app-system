import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import {RouterModule} from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  standalone: true,
  imports: [IonicModule,RouterModule
  
  ],
})
export class AppComponent  implements OnInit {

  profile:any;
  constructor() {}
  ngOnInit(): void {
    const profile = (localStorage.getItem('profile'));
    this.profile = profile;
     console.log(this.profile)
      
  }

  logout() {
    
    window.localStorage.clear();
    window.location.href = '/login';
  }

}
